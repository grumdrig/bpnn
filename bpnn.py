#!/usr/bin/python
# Back-Propagation Neural Networks
# 
# Based on public domain code by Neil Schemenauer <nas@arctrix.com>

import math
import random
import string
import operator


# Calculate a random number where:  a <= rand < b
def rand(a, b):
    return (b - a) * random.random() + a

# Our sigmoid function; tanh is a little nicer than the standard 1/(1+e^-x)
sigmoid = math.tanh

# Derivative of our sigmoid function, in terms of the output (i.e. y)
def dsigmoid(y):
    return 1.0 - y * y

def matrixMult(m, v):
    return [sum([v[i] * m[i][j] for i in range(len(v))])
            for j in range(len(m[0]))]


class NN:
    def __init__(self, inputs, hiddens, outputs):
        # Number of input, hidden, and output nodes
        self.ni = inputs + 1  # one more for bias node
        self.nh = hiddens
        self.no = outputs

        # Activations for nodes
        self.ai = [1.0] * self.ni
        self.ah = [1.0] * self.nh
        self.ao = [1.0] * self.no
        
        # Create weights with random vaules
        self.wi = [[rand(-0.2, 0.2) for j in self.ah] for i in self.ai]
        self.wo = [[rand(-2.0, 2.0) for k in self.ao] for j in self.ah]

        # Last change in weights for momentum   
        self.ci = [[0.0] * self.nh for i in self.ai]
        self.co = [[0.0] * self.no for i in self.ah]

    def update(self, inputs):
        assert len(inputs) == self.ni - 1

        # Input activations
        self.ai[:-1] = inputs

        # Hidden activations
        self.ah = map(sigmoid, matrixMult(self.wi, self.ai))

        # Output activations
        self.ao = map(sigmoid, matrixMult(self.wo, self.ah))

        return self.ao

    def backPropagate(self, targets, N, M):
        assert len(targets) == self.no

        # Calculate error terms for output
        output_deltas = [dsigmoid(o) * (t-o) for o,t in zip(self.ao, targets)]

        # Calculate error terms for hidden
        hidden_deltas = [dsigmoid(self.ah[j]) *
                         sum(map(operator.mul, output_deltas, self.wo[j]))
                         for j in range(self.nh)]

        # Update output weights
        for j in range(self.nh):
            for k in range(self.no):
                change = output_deltas[k] * self.ah[j]
                self.wo[j][k] = self.wo[j][k] + N * change + M * self.co[j][k]
                self.co[j][k] = change

        # Update input weights
        for i in range(self.ni):
            for j in range(self.nh):
                change = hidden_deltas[j] * self.ai[i]
                self.wi[i][j] = self.wi[i][j] + N * change + M * self.ci[i][j]
                self.ci[i][j] = change

        # Return error
        return sum(map(lambda x: 0.5 * (t-o)**2, zip(targets, self.ao)))

    def test(self, patterns):
        for p in patterns:
            print p[0], '->', self.update(p[0])

    def weights(self):
        print 'Input weights:'
        for i in range(self.ni):
            print self.wi[i]
        print
        print 'Output weights:'
        for j in range(self.nh):
            print self.wo[j]

    def train(self, patterns, iterations=1000, N=0.5, M=0.1):
        # N: learning rate
        # M: momentum factor
        for i in xrange(iterations):
            error = 0.0
            for inputs,targets in patterns:
                self.update(inputs)
                error += self.backPropagate(targets, N, M)
            if i % 100 == 0:
                print 'error %-14f' % error


def demo():
    random.seed(0)

    # Create a network
    n = NN(inputs=2, hiddens=2, outputs=1)

    # Teach network XOR function
    pat = [
        [[0,0], [0]],
        [[0,1], [1]],
        [[1,0], [1]],
        [[1,1], [0]]
    ]

    # Train it with some patterns
    n.train(pat)

    # Test it
    n.test(pat)


if __name__ == '__main__':
    demo()
